+++
title = "SHDH 2023"
publishdate = "2023-03-21"
date = "2023-03-25"
event_place = "Salón de redes de la FEI"
description = "Primer SHDH presencial en años!"
thumbnail = "/img/eventos/shdh-2023-03-25.jpg"
author = "categulario"
topics = []
turnout = 14
+++

Un Super Happy Dev House es un evento de hackers y makers para trabajar en sus
proyectos personales durante 12 horas (nosotros lo intentamos) mientras
convivimos y al final presentamos lo que hicimos en ese lapso. Es un buen
espacio para resolver dudas, compartir inquietudes y obtener retroalimentacion
de proyectos de todo tipo y forma.

¡En esta ocasión volvemos a la modalidad presencial! La dinámica es simple:
¿Tienes un proyecto personal de cualquier tipo y no encuentras el tiempo para
hacerlo? ¡Tráelo!

Para participar registra la actividad que estarás realizando [en este
documento](https://docs.google.com/spreadsheets/d/1v7kEB45JrRmcJWj5MGZUGWIszW40vpk4lWURVT--HUY/edit?usp=sharing).
También verás en qué actividades estarán trabajando otras personas.

## Sobre la sede

El evento se realizará en el Salón de Redes de la Facultad de Estadística e
Informática de la Universidad Veracruzana. Trae todo lo que necesites para
trabajar en tu proyecto y ¡ven a convivir!

## Sobre el evento

En un Super Happy Dev House los asistentes se dedican durante 12 horas (o hasta
que el cuerpo aguante) a desarrollar algún proyecto o varios, aprender algo
nuevo durante el evento, y en las últimas horas se invita a los asistentes a
pasar a platicar qué es lo que lograron durante el SHDH.

Un Super Happy Dev House no es un evento de networking, no es una competencia o
una conferencia (aunque se dan pláticas), no es una fiesta, aunque de alguna
manera se haga un poco de todo eso.
