+++
title = "Meetup 23 de Noviembre 2018"
publishdate = "2019-01-23"
date = "2018-11-23"
event_place = "Facultad de Instrumentación UV"
description = "Meetup en la facultad de instrumentación"
thumbnail = "/img/eventos/meetup-2018-11-23.jpg"
topics = ["Karel", "React Native"]
turnout = 20
+++

En esta ocasión la sede fue la facultad de instrumentación electrónica.

## Temas

* Karel [@categulario](https://twitter.com/categulario)
* React Native (José Adrián)
