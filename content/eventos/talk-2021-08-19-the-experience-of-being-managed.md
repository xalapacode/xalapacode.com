+++
title = "The experience of being managed"
publishdate = "2021-08-15"
date = "2021-08-19"
event_place = "Online"
description = "Una charla invitada por Michelle Torres"
thumbnail = "/img/eventos/talk-2021-08-19-the-experience-of-being-managed.jpg"
author = "categulario"
turnout = 24
draft = false
+++

Acompáñennos en esta edición especial de los encuentros, con una ponente
invitada que desde el otro lado del chargo nos viene a contar sus experiencias
trabajando con un manager.

## Sobre la sede

Debido a la contingencia sanitaria que se vive a nivel mundial nuestros últimos eventos han sido (y seguirán siendo) en línea, ¡así que ahora es más fácil que nunca asistir!

Sigue la transmisión en vivo por cualquiera de nuestros canales en punto de las 12:00 hrs tiempo de la Ciudad de México:

* [Youtube](https://www.youtube.com/watch?v=8wIp2IxJtHc)
* [Facebook live](https://www.facebook.com/158398994507356/posts/1532274053786503/)
* [Twitter](https://twitter.com/xalapacode)
