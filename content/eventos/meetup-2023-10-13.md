+++
title = "Encuentros Xalapacode Octubre"
publishdate = "2023-12-27"
date = "2023-10-13"
event_place = "Sala audiovisual de la Facultad de estadística e informática UV"
description = "Encuentro de Octubre de la comunidad"
thumbnail = "/img/eventos/meetup-2023-10-13.jpg"
author = "koffer"
draft = false
+++

## Sobre el evento

Ven a conocer (o saludar si ya nos conocemos) a la comunidad de Xalapacode!
Regresamos con un evento presencial En Octubre para vernos las caras y saludarnos. Sube al escenario y compártenos sobre algún
tema de tu interés, hablamos sobre proyectos que se fueron y ya no estan, y gente que se fue y ya no esta.

## Las pláticas

¿Con qué tema nos vas a sorprender esta vez?

## Sobre la sede

Se utilizará la sala audiovisual de la Facultad de Estadística e informática. La
cita es a las 19:00 hora central de México (UTC-6:00).

Ubicación de la FEI
https://goo.gl/maps/HrK9GnHB7XgycFnm6
