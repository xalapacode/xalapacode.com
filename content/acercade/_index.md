+++
title = "Acerca de"
description = "Acerca de xalapacode"
+++

Bienvenidos al sitio web oficial de la comunidad xalapacode, aquí encontraras información sobre tus preguntas.

## ¿Por qué?

Por que creemos que compartir, colaborar y convivir es una manera de mejorar como personas y profesionales.

## ¿Qué es?

Una comunidad de programadores, diseñadores y entusiastas de la tecnología que se reunen periódicamente en la ciudad de Xalapa, Veracruz.

## ¿Comó?

Reuniéndonos periódicamente para hablar sobre lo que hacemos en nuestros trabajos y pasatiempos, impartiendo talleres y conferencias entre otras actividades.

## ¿Donde descargo el logo?

Ve a [Identidad](/identidad) y podrás descargar nuestro logotipo y ver nuesta guia de estilo.

## Código de Conducta

Te invitamos a leer nuestro [Código de conducta](https://xalapacode.com/codigo-conducta)

## Participa

* Da una charla
* Difunde la comunidad
* Programa, diseña, construye o mejora algo
* Pregunta, siempre pregunta
* Adopta a un novato
* Contrata o ayuda a contratar a alguien
* Patrocina (café y galletas son siempre bienvenidos)
* Contribuye a este sitio

## Sitios externos

[Facebook](https://facebook.com/xalapacode)
[Gitlab](https://gitlab.com/xalapacode)
